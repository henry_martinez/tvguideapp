package com.martinez.tvguide

import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.martinez.tvguide.data.local.TvShowsDatabase
import com.martinez.tvguide.data.models.TvShowResult
import org.hamcrest.core.IsEqual.equalTo
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException


@RunWith(AndroidJUnit4::class)
class TvShowDbUnitTest {
    private lateinit var db: TvShowsDatabase

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getTargetContext()
        db = Room.inMemoryDatabaseBuilder(context, TvShowsDatabase::class.java)
                .allowMainThreadQueries()
                .build()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeUserAndReadInList() {
        val user = TvShowResult(11, name = "Game of thrones")
        db.tvShowsDao().insert(listOf(user))
        val byId = db.tvShowsDao().loadTvShowSync(11)
        assertThat(byId, equalTo(user))
    }

}