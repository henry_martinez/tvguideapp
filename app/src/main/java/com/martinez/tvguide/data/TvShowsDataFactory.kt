package com.martinez.tvguide.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.martinez.tvguide.data.models.TvShowResult
import io.reactivex.disposables.CompositeDisposable

class TvShowsDataFactory(
        private val tvId: Int,
        private val service: TvShowRepository,
        private val compositeDisposable: CompositeDisposable
) : DataSource.Factory<Long, TvShowResult>() {

    val sourceLiveData = MutableLiveData<SimilarTvShowsDataSource>()

    override fun create(): DataSource<Long, TvShowResult> {
        val tvShowsDataSource = SimilarTvShowsDataSource(tvId, service, compositeDisposable)
        sourceLiveData.postValue(tvShowsDataSource)
        return tvShowsDataSource
    }
}