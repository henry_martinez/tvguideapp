package com.martinez.tvguide.data.remote

import com.martinez.tvguide.data.models.PopularTvShowsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TvShowService {

    @GET("tv/popular")
    fun requestPopularTvShows(
            @Query("api_key") apiKey: String = RemoteConfig.TMDB_API_KEY,
            @Query("language") language: String? = null,
            @Query("page") page: Int? = null
    ): Single<PopularTvShowsResponse>

    @GET("tv/{tv_id}/similar")
    fun requestSimilarTvShows(
            @Path("tv_id") tvId: Int,
            @Query("api_key") apiKey: String = RemoteConfig.TMDB_API_KEY,
            @Query("language") language: String? = null,
            @Query("page") page: Int? = null
    ): Single<PopularTvShowsResponse>


}