package com.martinez.tvguide.data.local

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.martinez.tvguide.data.models.TvShowResult

@Dao
interface TvShowsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(tvShows: List<TvShowResult>)

    @Query("SELECT * FROM tv_show_results ORDER BY popularity DESC")
    fun tvShowsByPopularity(): DataSource.Factory<Int, TvShowResult>

    @Query("SELECT * FROM tv_show_results WHERE id = :id ")
    fun loadTvShow(id: Int): LiveData<TvShowResult>

    @Query("SELECT * FROM tv_show_results WHERE id = :id ")
    fun loadTvShowSync(id: Int): TvShowResult
}