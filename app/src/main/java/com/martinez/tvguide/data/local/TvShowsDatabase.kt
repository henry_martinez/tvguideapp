package com.martinez.tvguide.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.martinez.tvguide.data.models.TvShowResult
import com.martinez.tvguide.utils.AppExecutors
import com.martinez.tvguide.utils.DiskIOThreadExecutor
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executors
import javax.inject.Singleton

@Database(
        entities = [TvShowResult::class],
        version = 1,
        exportSchema = false
)
@TypeConverters(TvShowsTypeConverter::class)
abstract class TvShowsDatabase : RoomDatabase() {

    abstract fun tvShowsDao(): TvShowsDao
}

@Module
object LocalModule {

    private const val THREAD_COUNT = 3


    @Singleton
    @Provides
    @JvmStatic
    fun providesDatabase(context: Context): TvShowsDatabase {
        return Room.databaseBuilder(context.applicationContext,
                TvShowsDatabase::class.java, "Tmdb.db")
                .build()
    }

    @Singleton
    @Provides
    @JvmStatic
    fun provideTvShowsDao(db: TvShowsDatabase): TvShowsDao {
        return db.tvShowsDao()
    }

    @Singleton
    @Provides
    @JvmStatic
    fun provideAppExecutors(): AppExecutors {
        return AppExecutors(DiskIOThreadExecutor(),
                Executors.newFixedThreadPool(THREAD_COUNT),
                AppExecutors.MainThreadExecutor())
    }


}