package com.martinez.tvguide.data.models

import com.google.gson.annotations.SerializedName

data class PopularTvShowsResponse(
        @SerializedName("page") val page: Int,
        @SerializedName("results") val results: List<TvShowResult?>,
        @SerializedName("total_results") val totalResults: Int,
        @SerializedName("total_pages") val totalPages: Int
)