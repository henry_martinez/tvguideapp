package com.martinez.tvguide.data.remote

object RemoteConfig {

    const val BASE_API_URL = "https://api.themoviedb.org/3/"
    const val TMDB_API_KEY = "927593a537751b1307e3fec71b0a4ec3"
    const val BASE_IMAGE_URL = "https://image.tmdb.org/t/p/"
    const val IMAGE_POSTER_SIZE = "w342"
}