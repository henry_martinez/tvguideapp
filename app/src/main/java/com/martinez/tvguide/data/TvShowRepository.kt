package com.martinez.tvguide.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.martinez.tvguide.data.local.TvShowsLocalCache
import com.martinez.tvguide.data.models.PopularTvShowsResponse
import com.martinez.tvguide.data.models.TvShowResult
import com.martinez.tvguide.data.remote.TvShowService
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TvShowRepository @Inject constructor(
        private val service: TvShowService,
        private val cache: TvShowsLocalCache) {

    private val networkErrors = MutableLiveData<String>()

    private var isRequestInProgress = false


    fun loadTvShows(): TvShowsUIModel {
        val dataSourceFactory = cache.loadTvShows()

        val config = PagedList.Config.Builder()
                .setPageSize(DATABASE_PAGE_SIZE)
                .setInitialLoadSizeHint(DATABASE_PAGE_SIZE * 2)
                .setPrefetchDistance(50)
                .setEnablePlaceholders(false)
                .build()


        // Construct the boundary callback
        val boundaryCallback = TvShowBoundaryCallback(service, cache)
        val networkErrors = boundaryCallback.networkErrors

        val data = LivePagedListBuilder(dataSourceFactory, config)
                .setBoundaryCallback(boundaryCallback)
                .build()

        return TvShowsUIModel(data, networkErrors)
    }


    fun loadTvShowDetail(tvShowId: Int): LiveData<TvShowResult> {
        return cache.loadTvShowById(tvShowId)
    }

    fun requestSimilarTvShows(tvId: Int, page: Int): Single<PopularTvShowsResponse> {
        return service.requestSimilarTvShows(tvId, page = page)
    }


    companion object {
        private const val DATABASE_PAGE_SIZE = 100
    }
}

data class TvShowsUIModel(
        val data: LiveData<PagedList<TvShowResult>>,
        val networkErrors: LiveData<String>
)