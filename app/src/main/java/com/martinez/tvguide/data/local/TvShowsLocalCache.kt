package com.martinez.tvguide.data.local

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import com.martinez.tvguide.data.models.TvShowResult
import com.martinez.tvguide.utils.AppExecutors
import javax.inject.Inject

class TvShowsLocalCache @Inject constructor(
        private val repoDao: TvShowsDao,
        private val appExecutors: AppExecutors
) {

    fun insert(shows: List<TvShowResult>, insertFinished: () -> Unit) {
        appExecutors.diskIO().execute {
            Log.d("TvShowsLocalCache", "inserting ${shows.size} shows")
            repoDao.insert(shows)
            insertFinished()
        }
    }


    fun loadTvShows(): DataSource.Factory<Int, TvShowResult> {
        return repoDao.tvShowsByPopularity()
    }

    fun loadTvShowById(showId: Int): LiveData<TvShowResult> {
        return repoDao.loadTvShow(showId)
    }
}