package com.martinez.tvguide.data

import android.annotation.SuppressLint
import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.martinez.tvguide.data.models.PopularTvShowsResponse
import com.martinez.tvguide.data.models.TvShowResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class SimilarTvShowsDataSource(
        private val tvId: Int,
        private val showRepository: TvShowRepository,
        private val compositeDisposable: CompositeDisposable
) : PageKeyedDataSource<Long, TvShowResult>() {


    @SuppressLint("CheckResult")
    override fun loadInitial(
            params: LoadInitialParams<Long>,
            callback: LoadInitialCallback<Long, TvShowResult>
    ) {
        compositeDisposable.add(
                showRepository.requestSimilarTvShows(tvId = tvId, page = 1)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ response: PopularTvShowsResponse ->
                            callback.onResult(response.results, null, 2L)
                        }, {
                            Log.e("TvShowsDataSource", it.message)
                        }))
    }

    @SuppressLint("CheckResult")
    override fun loadAfter(
            params: LoadParams<Long>,
            callback: LoadCallback<Long, TvShowResult>
    ) {
        compositeDisposable.add(
                showRepository.requestSimilarTvShows(tvId = tvId, page = params.key.toInt())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ response: PopularTvShowsResponse ->
                            val nextKey = params.key + 1L
                            callback.onResult(response.results, nextKey)
                        }, {
                            Log.e("TvShowsDataSource", it.message)
                        }))
    }

    override fun loadBefore(
            params: LoadParams<Long>,
            callback: LoadCallback<Long, TvShowResult>
    ) = Unit
}