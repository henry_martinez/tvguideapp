package com.martinez.tvguide.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "tv_show_results")
data class TvShowResult(
        @PrimaryKey @field:SerializedName("id") val id: Int,
        @field:SerializedName("poster_path") val posterPath: String? = null,
        @field:SerializedName("popularity") val popularity: Double = 0.0,
        @field:SerializedName("backdrop_path") val backdropPath: String? = null,
        @field:SerializedName("vote_average") val voteAverage: Double = 0.0,
        @field:SerializedName("overview") val overview: String = "",
        @field:SerializedName("first_air_date") val firstAirDate: String = "",
        @field:SerializedName("origin_country") val originCountry: List<String> = emptyList(),
        @field:SerializedName("genre_ids") val genreIds: List<Int> = emptyList(),
        @field:SerializedName("original_language") val originalLanguage: String = "",
        @field:SerializedName("vote_count") val voteCount: Int = 0,
        @field:SerializedName("name") val name: String,
        @field:SerializedName("original_name") val originalName: String = ""
)