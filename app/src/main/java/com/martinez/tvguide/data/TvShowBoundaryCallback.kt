package com.martinez.tvguide.data

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.martinez.tvguide.data.local.TvShowsLocalCache
import com.martinez.tvguide.data.models.TvShowResult
import com.martinez.tvguide.data.remote.TvShowService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class TvShowBoundaryCallback(
        private val service: TvShowService,
        private val cache: TvShowsLocalCache
) : PagedList.BoundaryCallback<TvShowResult>() {

    private var lastRequestedPage = 1

    private val _networkErrors = MutableLiveData<String>()

    val networkErrors: LiveData<String>
        get() = _networkErrors

    private var isRequestInProgress = false

    override fun onZeroItemsLoaded() {
        requestAndSaveData()
    }

    override fun onItemAtEndLoaded(itemAtEnd: TvShowResult) {
        requestAndSaveData()
    }

    @SuppressLint("CheckResult")
    private fun requestAndSaveData() {
        if (isRequestInProgress) return

        isRequestInProgress = true

        service.requestPopularTvShows(page = lastRequestedPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    lastRequestedPage++
                    cache.insert(it.results.filterNotNull()) {
                        isRequestInProgress = false
                    }
                }, {
                    _networkErrors.postValue(it.message)
                    isRequestInProgress = false
                })

    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 20
    }
}