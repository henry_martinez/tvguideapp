package com.martinez.tvguide.ui.showdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.martinez.tvguide.data.TvShowRepository
import com.martinez.tvguide.data.TvShowsDataFactory
import com.martinez.tvguide.data.models.TvShowResult
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class ShowDetailViewModel @Inject constructor(
        private val repository: TvShowRepository
) : ViewModel() {

    val compositeDisposable = CompositeDisposable()

    fun loadTvShoeDetail(id: Int): LiveData<TvShowResult> {
        return repository.loadTvShowDetail(id)
    }

    fun loadSimilarTvShows(id: Int): LiveData<PagedList<TvShowResult>> {
        val pagedListConfig = PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(10)
                .setPageSize(20).build()

        val tvShowsDataFactory = TvShowsDataFactory(id, repository, compositeDisposable)

        return LivePagedListBuilder(tvShowsDataFactory, pagedListConfig)
                .build()
    }


    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}
