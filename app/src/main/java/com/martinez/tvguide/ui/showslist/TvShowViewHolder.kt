package com.martinez.tvguide.ui.showslist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.martinez.tvguide.R
import com.martinez.tvguide.data.models.TvShowResult
import com.martinez.tvguide.data.remote.RemoteConfig
import com.squareup.picasso.Picasso

class TvShowViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    val title: TextView = view.findViewById(R.id.textView_show_title)
    val voteAverage: TextView = view.findViewById(R.id.textView_vote_average)
    val imagePoster: ImageView = view.findViewById(R.id.imageView_show_poster)

    fun bind(tvShow: TvShowResult?, itemClick: ((TvShowResult) -> Unit)?) {
        if (tvShow == null) {
            title.text = itemView.resources.getString(R.string.copy_loading)
        } else {
            showTvShowData(tvShow)
            itemView.setOnClickListener { itemClick?.invoke(tvShow) }
        }
    }

    private fun showTvShowData(tvShow: TvShowResult) {
        title.text = tvShow.name
        voteAverage.text = tvShow.voteAverage.toString()
        val imageUrl = RemoteConfig.BASE_IMAGE_URL + RemoteConfig.IMAGE_POSTER_SIZE + tvShow.posterPath
        Picasso.get().load(imageUrl).into(imagePoster)
    }

    companion object {
        fun create(parent: ViewGroup): TvShowViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_tv_show, parent, false)
            return TvShowViewHolder(view)
        }
    }
}