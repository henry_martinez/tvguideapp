package com.martinez.tvguide.ui.showdetail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.martinez.tvguide.R
import com.martinez.tvguide.data.models.TvShowResult
import com.martinez.tvguide.data.remote.RemoteConfig
import com.martinez.tvguide.di.FragmentScope
import com.martinez.tvguide.ui.showslist.ShowListAdapter
import com.martinez.tvguide.utils.SpacesItemDecoration
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.show_detail_fragment.*
import javax.inject.Inject

class ShowDetailFragment : Fragment() {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ShowDetailViewModel

    private val showListAdapter = ShowListAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.show_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val showId: Int = arguments?.getInt(TV_SHOW_ID) ?: -1

        setupRecyclerView()

        viewModel = ViewModelProviders.of(this, viewModelFactory)[ShowDetailViewModel::class.java]

        if (showId != -1) {
            viewModel.loadTvShoeDetail(showId).observe(this, Observer(::bindTvShow))

            viewModel.loadSimilarTvShows(showId).observe(this, Observer(::bindSimilarShows))
        } else {
            Toast.makeText(context?.applicationContext, "Invalid TvShow", Toast.LENGTH_LONG).show()
            activity?.supportFragmentManager?.popBackStack()
        }
    }

    private fun bindSimilarShows(it: PagedList<TvShowResult>?) {
        showListAdapter.submitList(it)
    }

    private fun bindTvShow(it: TvShowResult) {
        tv_show_title.text = it.name
        tv_show_description.text = it.overview
        val imageUrl = RemoteConfig.BASE_IMAGE_URL + RemoteConfig.IMAGE_POSTER_SIZE + it.backdropPath
        Picasso.get().load(imageUrl).into(imageView_show_poster)
    }

    private fun setupRecyclerView() {
        recyclerView_similar_shows.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        recyclerView_similar_shows.addItemDecoration(SpacesItemDecoration(resources.getDimensionPixelOffset(R.dimen.items_spacing)))
        recyclerView_similar_shows.adapter = showListAdapter
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    companion object {
        private const val TV_SHOW_ID = "TV_SHOW_ID"
        fun newInstance(id: Int): ShowDetailFragment {
            val bundle = Bundle()
            bundle.putInt(TV_SHOW_ID, id)
            val showDetailFragment = ShowDetailFragment()
            showDetailFragment.arguments = bundle
            return showDetailFragment
        }
    }

}

@Module
abstract class ShowDetailFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindShowDetailFragment(): ShowDetailFragment
}

