package com.martinez.tvguide.ui.showslist

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.GridLayoutManager
import com.martinez.tvguide.MainActivity
import com.martinez.tvguide.R
import com.martinez.tvguide.data.models.TvShowResult
import com.martinez.tvguide.di.FragmentScope
import com.martinez.tvguide.utils.SpacesItemDecoration
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.shows_list_fragment.*
import javax.inject.Inject

class ShowsListFragment : Fragment() {

    companion object {
        fun newInstance() = ShowsListFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ShowsListViewModel

    private val adapter = ShowListAdapter { tvShowResult ->
        (activity as MainActivity).navigateToTvShowDetail(tvShowResult)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.shows_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[ShowsListViewModel::class.java]
        initAdapter()


        viewModel.showsList.observe(this, Observer(::updateShows))
    }

    private fun initAdapter() {
        recyclerView_tv_shows.apply {
            layoutManager = GridLayoutManager(context, 3)
            addItemDecoration(SpacesItemDecoration(resources.getDimensionPixelOffset(R.dimen.items_spacing)))
            this.adapter = this@ShowsListFragment.adapter
        }

    }

    private fun updateShows(pagedList: PagedList<TvShowResult>) {
        Log.d("ShowsListFragment", "list: ${pagedList.size}")
        adapter.submitList(pagedList)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

}

@Module
abstract class ShowsListFragmentModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun bindShowsListFragment(): ShowsListFragment
}
