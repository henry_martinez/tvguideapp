package com.martinez.tvguide.ui.showslist

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.martinez.tvguide.data.TvShowRepository
import com.martinez.tvguide.data.models.TvShowResult
import javax.inject.Inject

class ShowsListViewModel @Inject constructor(private val repository: TvShowRepository) : ViewModel() {

    val showsList: LiveData<PagedList<TvShowResult>> = repository.loadTvShows().data

}
