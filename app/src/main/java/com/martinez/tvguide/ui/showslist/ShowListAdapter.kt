package com.martinez.tvguide.ui.showslist

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.martinez.tvguide.data.models.TvShowResult


class ShowListAdapter(private val itemClick: ((TvShowResult) -> Unit)? = null
) : PagedListAdapter<TvShowResult, RecyclerView.ViewHolder>(
        DIFF_CALLBACK) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TvShowViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val tvSHowItem = getItem(position)
        if (tvSHowItem != null) {
            (holder as TvShowViewHolder).bind(tvSHowItem, itemClick)
        }
    }

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TvShowResult>() {

            override fun areItemsTheSame(oldItem: TvShowResult, newItem: TvShowResult) = oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: TvShowResult, newItem: TvShowResult) =
                    oldItem == newItem
        }
    }
}