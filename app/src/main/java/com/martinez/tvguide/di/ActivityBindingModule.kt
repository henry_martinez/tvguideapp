package com.martinez.tvguide.di

import com.martinez.tvguide.MainActivity
import com.martinez.tvguide.ui.showdetail.ShowDetailFragmentModule
import com.martinez.tvguide.ui.showslist.ShowsListFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [ShowsListFragmentModule::class, ShowDetailFragmentModule::class])
    @ActivityScope
    abstract fun bindMainActivity(): MainActivity

}