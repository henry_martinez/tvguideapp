package com.martinez.tvguide.di

import javax.inject.Scope

@Scope
@Retention
annotation class ActivityScope

@Scope
@Retention
annotation class FragmentScope