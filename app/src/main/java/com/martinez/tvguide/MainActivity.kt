package com.martinez.tvguide

import android.os.Bundle
import com.martinez.tvguide.data.models.TvShowResult
import com.martinez.tvguide.ui.showdetail.ShowDetailFragment
import com.martinez.tvguide.ui.showslist.ShowsListFragment
import dagger.android.support.DaggerAppCompatActivity

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, ShowsListFragment.newInstance())
                    .commitNow()
        }
    }

    fun navigateToTvShowDetail(tvShow: TvShowResult) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, ShowDetailFragment.newInstance(tvShow.id))
                .addToBackStack(null)
                .commit()
    }

}
